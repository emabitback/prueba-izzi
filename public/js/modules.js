console.log("#Load");



var Login = {
		
		init: url => {
			
			$('.select2').select2();
			
			let token = $('meta[name="csrf-token"]').attr('content');
			
			$.ajax({
				url: url,
			    headers: {
			        'X-CSRF-TOKEN': token
			    },
				data: null,
				type: 'POST',
				dataType: 'json',
				success: function(data) {
					var option = new Option("Selecciona una opción", -1, false, false);
			        $('#perfil').append(option);
					
					for (var i = 0; i < data.length; i++) {
						var item = data[i];
						var option = new Option(item.profile_name, item.id, false, false);
						$('#perfil').append(option);
					}
				}
			});
		}
		
};

var Product = {
		
		init: (url1, url2) => {
			
			let token = $('meta[name="csrf-token"]').attr('content');
			
			$.ajax({
				url: url1,
			    headers: {
			        'X-CSRF-TOKEN': token
			    },
				data: null,
				type: 'POST',
				dataType: 'json',
				success: function(data) {
					var option = new Option("Selecciona una opción", -1, false, false);
			        $('#categoria').append(option);
					
					for (var i = 0; i < data.length; i++) {
						var item = data[i];
						var option = new Option(item.name_category, item.id, false, false);
						$('#categoria').append(option);
					}
				}
			});




			$.ajax({
				url: url2,
			    headers: {
			        'X-CSRF-TOKEN': token
			    },
				data: null,
				type: 'POST',
				dataType: 'json',
				success: function(data) {
					var option = new Option("Selecciona una opción", -1, false, false);
			        $('#sucursal').append(option);
					
					for (var i = 0; i < data.length; i++) {
						var item = data[i];
						var option = new Option(item.name_office, item.id, false, false);
						$('#sucursal').append(option);
					}
				}
			});
		},
		
		
		register: url => {
			let data = $('#formProduct').serialize();
			console.log(data);
			
			
			let token = $('meta[name="csrf-token"]').attr('content');
			
			$.ajax({
				url: url,
			    headers: {
			        'X-CSRF-TOKEN': token
			    },
				data: data,
				type: 'POST',
				dataType: 'json',
				success: function(data) {
					console.log(data);
					
					$('#formProduct').reset();
				}
			});
		},
		
		
		showAll: url => {
			
			let token = $('meta[name="csrf-token"]').attr('content');
			
           $('#productsTable').DataTable({
            	data: null,
    			ajax: {
    		        url: url,
    		        headers: {
    			        'X-CSRF-TOKEN': token
    			    },
    			    type: "post",
    		        dataSrc: ""
    		    },
    		    
				processing: true,
    		    serverSide: false,
                lengthChange: false,
                responsive: true,
                scrollX: true,
                columns: [
                    {
						title: "producto", className: "text-center", data: "product"
					},
					{
						title: "Descripcion", className: "text-center", data: "descripcion"
					},
					{
						title: "Precio de Compra", className: "text-center",
						render : function (data, type, row, meta) {
							return '$'+ row.price;
                        }
					},
					
                ]
            });
		}
		
};


console.log("#BeforeLoad");