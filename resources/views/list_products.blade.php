@extends('layouts.app') 

@section('content')
<div class="container">
	<div class="row">
		<div class="offset-md-3 col-md-6">
			<div class="row">
				<table id="productsTable" class="table table-hover" cellspacing="0" width="100%"></table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		Product.showAll('{{ url('/product/all') }}');
	});
</script>
@endsection
